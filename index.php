<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
require 'Person.php';

print 'inizio<br>';
$page = 1;
$dove="Corigliano-Rossano%20(CS)";
$filename='test';
$url = "https://www.paginebianche.it/persone?qs=**&dv=".$dove;

print "sto scaricando pagina: {$page}<br>";
$COSTANTE = new COSTANTE();
$contents = file_get_contents($url . "&p={$page}");
$dom = new DOMDocument();
@$dom->loadHTML($contents);
$xpath = new DOMXPath($dom);
$nodi = $xpath->query($COSTANTE->nodeXPath);
$element = $xpath->query($COSTANTE->elementtXPath);
if ($element->length > 0) {
    $totElement = $element[0]->nodeValue;
    $elementNumberInDecimal = $totElement / 20;
    $pageNumber = round($elementNumberInDecimal, 0, PHP_ROUND_HALF_DOWN);
    if ($elementNumberInDecimal > $pageNumber) {
        $pageNumber = $pageNumber + 1;
    }
    print "tot person: {$totElement}; page number: {$pageNumber}<br>";
}

$fp = fopen($filename.'.csv', 'w');
addToCSV1($nodi, $fp, $xpath);
for ($i = 2; $i <= $pageNumber; $i++) {

    $contents = file_get_contents($url . "&p={$i}");
    $dom = new DOMDocument();
    @$dom->loadHTML($contents);
    $xpath = new DOMXPath($dom);
    $nodi = $xpath->query($COSTANTE->nodeXPath);
    print "sto scaricando pagina: {$i}<br>";
    $personaList = downloadPage($nodi, $xpath);
    print "pagina {$i} scairca. <br>";
    addToCSV($fp, $personaList);
    print "pagina {$i} inserita nel csv. <br>";

}
fclose($fp);
print "fine<br>";

function addToCSV1($nodi, $fp, $xpath)
{
    $listName = array('Nome', 'Indirizzo', 'citta', 'cap', 'Tel');
    $personList = downloadPage($nodi, $xpath);
    print "pagina 1 scarica. <br>";
    fputcsv($fp, $listName, ';');
    foreach ($personList as $fields) {
        fputcsv($fp, array($fields->nameSurname, $fields->address, $fields->city, $fields->cap, $fields->phone), ';');
    }
    print "pagina 1 inserita nel csv. <br>";

}

function addToCSV($fp, $personList)
{
    foreach ($personList as $fields) {
        fputcsv($fp, array($fields->nameSurname, $fields->address, $fields->city, $fields->cap, $fields->phone), ';');
    }

}

function downloadPage($nodi, $xpath)
{

    $COSTANTE = new COSTANTE();
    $personList = [];
    foreach ($nodi as $row) {
        $person = new Person();
        $person->nameSurname = (($xpath->query($COSTANTE->nameXPath, $row)->length > 0) ? trim($xpath->query($COSTANTE->nameXPath, $row)[0]->nodeValue) : NULL);

        $addressDivHtml = $xpath->query($COSTANTE->addressXPath, $row);
        $person->address = $addressDivHtml->length > 0 ? trim($addressDivHtml[0]->nodeValue) : null;
        $capHtml = $xpath->query($COSTANTE->capXPath, $row);
        $person->cap = ($capHtml->length > 0) ? trim($capHtml[0]->nodeValue) : NULL;
        $addressDivHtml = $xpath->query($COSTANTE->addressXPath, $row);
        $person->city = ($xpath->query($COSTANTE->localityXPath, $row)->length > 0) ? trim(($xpath->query($COSTANTE->localityXPath, $row))[0]->nodeValue) : NULL;
        $person->phone = ($xpath->query($COSTANTE->phoneXPath, $row)->length > 0) ? trim(($xpath->query($COSTANTE->phoneXPath, $row))[0]->nodeValue) : NULL;
        array_push($personList, $person);
    }

    return $personList;
}

class COSTANTE
{
    public $nodeXPath = "//div[contains(@class,\"vcard\")]";
    public $addressXPath = ".//span[@itemprop=\"streetAddress\"]";
    public $capXPath = ".//span[@itemprop=\"postalCode\"]";
    public $localityXPath = ".//span[@itemprop=\"addressLocality\"]";
    public $phoneXPath = ".//span[@itemprop=\"telephone\"]";

    public $nameXPath = ".//h2[contains(@class,\"rgs\")]/a";
    public $elementtXPath = ".//p[contains(@class,\"listing-n-result\")]/b";
}

?>